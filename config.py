PROXMOX_HOST = {
    'host': 'IP',
    'user': 'USER@pam',
    'password': "PASSWORD",
    'verify_ssl': False
}

# drs_aggression: 1 - 10
# drs_aggression is used to loop n times per run of the script
# i.e. can migration up to n VMs per script run
# The script checks the nodes load between each loop to monitor for balance

# top_vms: 1 - n
# top_vms is used to select the n-th top VM to migrate to the destination node

# min_vms: 1 - n
# min_vms is the minimum number of VMs to leave on a node even if the node 
# has high CPU

# maint_path: path
# maint_path is the path to where the .maintenance files will be created.
# The .maintenance files trigger a migration of all VMs of that host.
# e.g chc1-pve-03.maintenance will force a migration of all VMs off the node chc1-pve-03
# While the .maintenance file exists, that node will not be included in load balancing

RESOURCE_SCHEDULER = {
    'drs_aggression': 5,
    'top_vms': 5,
    'min_vms': 1,
    'maint_path': '/pve/maintenance/'
}

